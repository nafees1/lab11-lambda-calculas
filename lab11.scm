

(define l_true  (lambda (m) (lambda (n) (OR (is-zero m) (less_than m n)) (myloop n s))));for if
(define l_false (lambda (m) (lambda (n) (add m n))))  ;;for else


(define l_if
    (lambda (mcond)               ;checking condition
        (lambda (if-tr)
            (lambda (if-fal)
                ((mcond if-tr) if-fal)
            )
        )
    )
)
(define s 0)
(define i 1)
(define n 100)

(define myloop 
   (lambda (n i) 
     (set! i (+ i 1) add(i s))  ;;adding i to s
        (if (<= i n)
            (myloop)
        )
   ) 
 ) 


(define add 
   (lambda (x y) 
     (m x (n y z)) 
   ) 
 ) 

(define is-zero
    (lambda (n)
            ((n (lambda (x) lfalse)) ltrue)
    )
)

(define pred_pair
    (lambda (pair)
        ((lcons
            (lcdr pair))
            (succ (lcdr pair)))
    )
)
(define pred
    (lambda (n)
        (lcar
            ((n pred_pair)
               
                ((lcons zero) zero)
            )
        )
    )
)

(define subtract
    (lambda (n)
        (lambda (m)
            ((m pred) n)
        )
    )
)

(define less_than 
   (lambda (a b) 
     (lambda(m n)(is-zero(subtract m n))) 
   ) 
 ) 


;reference: wikipedia
; reference: shlomifish.org